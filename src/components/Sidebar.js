import { List } from './List/List'
import { Item } from './List/Item'

export const Sidebar = () => {
  const data = [
    {
      className: ['link', 'red-text'],
      link: {
        href: 'https://ru.reactjs.org/docs/react-api.html#cloneelement',
        title: 'React CloneElement',
        target: '_blank'
      }
    },
    {title:'Я михал палыч тереньтьев'},
    {text:'Пьяный'}
  ]
  return (
    <div className="sidebar">
      <List array={data}>
        <Item/>
      </List>
    </div>
  )
}
