import { List } from './List/List'
import { Item } from './List/Item'

export const Content = () => {
  const data = [
    { className: ['content-title'], title: 'Ты куда звОнишь?' },
    { className:'width100', img: 'https://memepedia.ru/wp-content/uploads/2020/06/nu-kak-tam-s-dengami-mem-3-1.jpg' },
    { className: ['content-title', 'red-text'], text: 'сынок @баный' }
  ]
  return (
    <div className="content">
      <List array={data}>
        <Item/>
      </List>
    </div>
  )
}
