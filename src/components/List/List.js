import { cloneElement } from 'react'

export const List = ({ children, array }) => {
  return (
    <>
      {array.map(item => cloneElement(children, {item}))}
    </>
  )
}
