export const Item = ({ item }) => {
  return (
    <div className={typeof item.className === 'object' ? item.className.join(' ') : item.className}>
      {item.title && <h2 className="title">{item.title}</h2>}
      {item.img && <img className="img" src={item.img} alt="img"/>}
      {item.text && <div className="text">{item.text}</div>}
      {item.link && <a href={item.link.href}
                       className={typeof item.className === 'object' ? item.className.join(' ') : item.className}
                       target={item.link.target}>{item.link.title}</a>}
    </div>
  )
}
