export const MainContainer = ({children}) =>{
  return (
    <div className='main'>
      {children}
    </div>
  )
}
