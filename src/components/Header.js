import { List } from './List/List'
import { Item } from './List/Item'

export const Header = () => {
  const data = [
    { className: ['header-test', 'red-text'], text: 'Ну' },
    { className: 'header-test', text: 'как' },
    { className: ['header-test', 'red-text'], text: 'там с ' },
    { className: ['header-test', 'green-text'], text: 'деньгами обстоит' },
    { className: ['header-test', 'red-text'], text: 'вопрос' }
  ]
  return (
    <div className="header">
      <List array={data}>
        <Item/>
      </List>
    </div>
  )
}
