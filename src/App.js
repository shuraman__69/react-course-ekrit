import logo from './logo.svg';
import './App.css';
import { Sidebar } from './components/Sidebar'
import { MainContainer } from './components/MainContainer'
import { Header } from './components/Header'
import { Content } from './components/Content'

function App() {
  return (
    <div className="App">
      <Sidebar/>
      <MainContainer>
        <Header/>
        <Content/>
      </MainContainer>
    </div>
  );
}

export default App;
